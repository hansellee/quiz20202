﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam2020_2.Entities.Entities
{
    public partial class TbTugas
    {
        public TbTugas()
        {
            TbMengerjakan = new HashSet<TbMengerjakan>();
        }

        [Key]
        public int TugasId { get; set; }
        [Required]
        [StringLength(255)]
        public string TugasName { get; set; }

        [InverseProperty("Tugas")]
        public virtual ICollection<TbMengerjakan> TbMengerjakan { get; set; }
    }
}
