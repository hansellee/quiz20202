﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam2020_2.Entities.Entities
{
    public partial class TbMahasiswa
    {
        public TbMahasiswa()
        {
            TbMengerjakan = new HashSet<TbMengerjakan>();
        }

        [Key]
        public int MahasiswaId { get; set; }
        [Required]
        [StringLength(255)]
        public string MahasiswaName { get; set; }

        [InverseProperty("Mahasiswa")]
        public virtual ICollection<TbMengerjakan> TbMengerjakan { get; set; }
    }
}
