CREATE TABLE "TbMahasiswa"(
		"MahasiswaId" INT GENERATED ALWAYS AS IDENTITY NOT NULL,
		"MahasiswaName" VARCHAR(255) NOT NULL,
	CONSTRAINT "PK_TbMahasiswa" PRIMARY KEY ("MahasiswaId")
)

CREATE TABLE "TbTugas"(
		"TugasId" INT GENERATED ALWAYS AS IDENTITY NOT NULL,
		"TugasName" VARCHAR(255) NOT NULL,
	CONSTRAINT "PK_TbTugas" PRIMARY KEY ("TugasId")
)

CREATE TABLE "TbMengerjakan"(
		"MengerjakanId" INT GENERATED ALWAYS AS IDENTITY NOT NULL,
		"MengerjakanName" VARCHAR(255) NOT NULL,
		"MengerjakanJumlah" INT NOT NULL,
		"MengerjakanDate" TIMESTAMPTZ NOT NULL,
		"MahasiswaId" INT NOT NULL,
		"TugasId" INT NOT NULL,
		"CreatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
		"CreatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
		"UpdatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
		"UpdatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
	CONSTRAINT "PK_TbMengerjakan" PRIMARY KEY ("MengerjakanId"),
	CONSTRAINT "FK_TbMengerjakan_TbMahasiswa" FOREIGN KEY ("MahasiswaId") REFERENCES "TbMahasiswa"("MahasiswaId"),
	CONSTRAINT "FK_TbMengerjakan_TbTugas" FOREIGN KEY ("TugasId") REFERENCES "TbTugas"("TugasId")
)