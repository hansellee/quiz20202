﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Exam2020_2.Entities.Entities
{
    public partial class ActivityDbContext : DbContext
    {
        
        public ActivityDbContext(DbContextOptions<ActivityDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbMahasiswa> TbMahasiswa { get; set; }
        public virtual DbSet<TbMengerjakan> TbMengerjakan { get; set; }
        public virtual DbSet<TbTugas> TbTugas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbMahasiswa>(entity =>
            {
                entity.Property(e => e.MahasiswaId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbMengerjakan>(entity =>
            {
                entity.Property(e => e.MengerjakanId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.Mahasiswa)
                    .WithMany(p => p.TbMengerjakan)
                    .HasForeignKey(d => d.MahasiswaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbMengerjakan_TbMahasiswa");

                entity.HasOne(d => d.Tugas)
                    .WithMany(p => p.TbMengerjakan)
                    .HasForeignKey(d => d.TugasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbMengerjakan_TbTugas");
            });

            modelBuilder.Entity<TbTugas>(entity =>
            {
                entity.Property(e => e.TugasId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
