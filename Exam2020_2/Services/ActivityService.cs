﻿using Exam2020_2.Entities.Entities;
using Exam2020_2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam2020_2.Services
{
    public class ActivityService
    {
        private readonly ActivityDbContext _db;

        public ActivityService(ActivityDbContext activityDbContext)
        {
            this._db = activityDbContext;
        }
        public async Task<List<ViewActivityModel>> GetAllActivity()
        {
            var query = from m in _db.TbMengerjakan
                        join mhw in _db.TbMahasiswa
                        on m.MahasiswaId equals mhw.MahasiswaId
                        join t in _db.TbTugas
                        on m.TugasId equals t.TugasId
                        select new ViewActivityModel
                        {
                            MengerjakanId = m.MengerjakanId,
                            MengerjakanDate = m.MengerjakanDate,
                            MahasiswaName = mhw.MahasiswaName,
                            TugasName = t.TugasName,
                            MengerjakanJumlah = m.MengerjakanJumlah
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();
            return data;
        }

        public async Task<bool> InsertActivity(AddActivityModel addActivityModel)
        {
            this._db.Add(new TbMengerjakan
            {
                MengerjakanId = addActivityModel.MengerjakanId,
                MengerjakanName = addActivityModel.MengerjakanName,
                MengerjakanDate = addActivityModel.MengerjakanDate,
                MahasiswaId = addActivityModel.MahasiswaId,
                TugasId = addActivityModel.TugasId,
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteActivity(int mengerjakanId)
        {
            var activityModel = await this._db
                .TbMengerjakan
                .Where(Q => Q.MengerjakanId == mengerjakanId)
                .FirstOrDefaultAsync();

            if (activityModel == null)
            {
                return false;
            }

            this._db.Remove(activityModel);
            await this._db.SaveChangesAsync();
            return true;
        }
    }
}
