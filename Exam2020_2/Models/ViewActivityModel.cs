﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam2020_2.Models
{
    public class ViewActivityModel
    {
        /// <summary>
        /// ID mengerjakan
        /// </summary>
        public int MengerjakanId { get; set; }
        /// <summary>
        /// Tanggal mengerjakan
        /// </summary>
        public DateTime? MengerjakanDate { get; set; }
        /// <summary>
        /// Nama mahasiswa yang mengerjakan tugas
        /// </summary>
        public string MahasiswaName { get; set; }
        /// <summary>
        /// Nama tugas yang dikerjakan
        /// </summary>
        public string TugasName { get; set; }
        /// <summary>
        /// Jumlah tugas yang dikerjakan
        /// </summary>
        public int MengerjakanJumlah { get; set; }
    }
}
