﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam2020_2.Entities.Entities
{
    public partial class TbMengerjakan
    {
        [Key]
        public int MengerjakanId { get; set; }
        [Required]
        [StringLength(255)]
        public string MengerjakanName { get; set; }
        public int MengerjakanJumlah { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime? MengerjakanDate { get; set; }
        public int MahasiswaId { get; set; }
        public int TugasId { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey(nameof(MahasiswaId))]
        [InverseProperty(nameof(TbMahasiswa.TbMengerjakan))]
        public virtual TbMahasiswa Mahasiswa { get; set; }
        [ForeignKey(nameof(TugasId))]
        [InverseProperty(nameof(TbTugas.TbMengerjakan))]
        public virtual TbTugas Tugas { get; set; }
    }
}
