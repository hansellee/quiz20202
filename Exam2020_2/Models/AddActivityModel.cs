﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam2020_2.Models
{
    public class AddActivityModel
    {
        public int MengerjakanId { get; set; }
        public string MengerjakanName { get; set; }
        public DateTime? MengerjakanDate { get; set; }
        public int MahasiswaId { get; set; }
        public int TugasId { get; set; }
 
    }
}
