﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam2020_2.Models;
using Exam2020_2.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam2020_2.APIs
{
    [Route("api/v1/activity")]
    [ApiController]
    public class ActivityAPIController : Controller
    {
        private readonly ActivityService _activityMan;

        public ActivityAPIController(ActivityService activityService)
        {
            this._activityMan = activityService;
        }
        // get all data
        [HttpGet("get-all-activity", Name = "getAllActivity")]
        public async Task<ActionResult<List<ViewActivityModel>>> GetAllActivityApi()
        {
            var data = await _activityMan.GetAllActivity();
            return Ok(data);
        }

        // insert activity
        [HttpPost("insert-activity", Name = "insertActivity")]
        public async Task<ActionResult<ResponseModel>> InsertActivityAsync([FromBody]AddActivityModel value)
        {
            var isSuccess = await this._activityMan.InsertActivity(value);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to insert"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert activity"
            });
        }

        // delete activity
        [HttpDelete("delete-activity", Name = "deleteActivity")]
        public async Task<ActionResult<ResponseModel>> DeleteTransactionAsync([FromBody] ViewActivityModel activityModel)
        {
            var isSuccess = await this._activityMan.DeleteActivity(activityModel.MengerjakanId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to delete"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success to delete transaction {activityModel.MengerjakanId}"
            });
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
